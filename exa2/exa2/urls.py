"""exa2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from cundinas import views
from views import Cundina_list, Turnos_list, Grupos_list, Contribuyentes_list,Aportes_list, grupo_new, contribuyente_new,cundina_new,gerente_new
app_name = "Cundina"

urlpatterns = [

    url(r'^admin/', admin.site.urls),
    url('', views.index, name = "index_view" ),
    
    url('inicio/', name="inicio"),
    url('login/', auth_views.Login.as_view(template_name="cundinas/login.html"), name="login"),
    url('Registro_/', auth_views.gerente_new.as_view(template_name="cundinas/Register.html"), name="Registrar Gerente"),
    url('Registro_Cundina/', auth_views.cundina_new.as_view(template_name="cundinas/Registro_Cundina.html"), name="Registrar Cundina"),
    url('Registro_Contribuyentes/', auth_views.contribuyentes_new.as_view(template_name="cundinas/Registro_Contribuyentes.html"), name="Registrar Contribuyentes"),
    url('Registro_Grupo/', auth_views.grupo_new.as_view(template_name="cundinas/Registro_Grupo.html"), name="Registrar Grupo"),

    url('Aportes/', views.aportes_list.as_view(template_name="cundinas/Aportes.html"), name="Aportes"),
    url('Cundinas/', views.cundinas_list.as_view(template_name="cundinas/Cundinas.html"), name="Cundinas"),
    url('Contribuyentes/', views.contribuyentes_list.as_view(template_name="cundinas/Contribuyentes.html"), name="Contribuyentes"),
    url('Grupos/', views.Grupos_list.as_view(template_name="cundinas/Grupos.html"), name="Grupos"),
    url('Turnos/', views.turnos_list.as_view(template_name="cundinas/Turnos.html"), name="Turnos"),

]
