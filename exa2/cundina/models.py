# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Cundina (models.Model):

    Nombre  = models.CharField(max_length = 100)
    Monto   = models.CharField(max_length = 70)
    Grupo   = models.CharField(max_length = 70)
    FechaI = models.CharField(max_length = 70)
    FechaT = models.CharField(max_length = 70)


    def __str__(self):
        return self.Nombre


class Contribuyente (models.Model):

    Nombre  = models.CharField(max_length = 100)
    Nombre_de_Usuario  = models.CharField(max_length = 100)
    Grupo   = models.CharField(max_length = 70)
    Aval = models.CharField(max_length = 70)
    Turno = models.CharField(max_length = 70)


    def __str__(self):
        return self.Nombre

class Aportes (models.Model):

    Nombre_Cundina  = models.CharField(max_length = 100)
    Nombre_Contribuyente  = models.CharField(max_length = 100)
    Monto   = models.CharField(max_length = 70)
    Grupo   = models.CharField(max_length = 70)
    Aporte = models.CharField(max_length = 70)
   Adeudo  = models.CharField(max_length = 70)


    def __str__(self):
        return self.Nombre



class Grupo (models.Model):

    Nombre  = models.CharField(max_length = 100)
    Nombre_de_Contribuyente1  = models.CharField(max_length = 100)
    Nombre_de_Contribuyente2  = models.CharField(max_length = 100)
    Nombre_de_Contribuyente3  = models.CharField(max_length = 100)


    def __str__(self):
        return self.Nombre
