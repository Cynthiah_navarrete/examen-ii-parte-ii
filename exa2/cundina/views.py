# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render
from django.urls import reverse_lazy

from django.views import generic
from django.contrib.auth.mixins import LoginRequiredMixin

from django.http import JsonResponse, HttpResponse
from django.core import serializers

from .models import Cundina, Contribuuyente, Grupo, Aportes
from .forms import Registro_Cundina, Cundinas, Registro_Contribuyente, Registro Grupo

# Create your views here.

class cundina_new(LoginRequiredMixin, generic.CreateView):

    template_name = "inv/Registro_Cundina.html"
    model = Cundina
    context_object_name = "obj"
    form_class = Registro_Cundina
    login_url = "cundinas:login"
    success_url = reverse_lazy("inv:Cundnas")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

class cundina_list(LoginRequiredMixin, generic.ListView):
    template_name = "inv/Cundina.html"
    model = Cundina
    context_object_name = "obj"
    login_url = "cundinas:login"

class cundinas_update(LoginRequiredMixin, generic.UpdateView):
    template_name = "inv/Cundinas.html"
    model = Cundinas
    context_object_name = "obj"
    form_class = Cundinas
    login_url = "cundinas:login"
    success_url = reverse_lazy("inv: Cundina")


    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)


class Contribuyente_new(LoginRequiredMixin, generic.CreateView):

    template_name = "inv/Registro_contribuyente.html"
    model = Contribuyente
    context_object_name = "obj"
    form_class = Registro_Contribuyente
    login_url = "cundinas:login"
    success_url = reverse_lazy("inv:Contribuyentes")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

class Contribuyente_list(LoginRequiredMixin, generic.ListView):
    template_name = "inv/Contribuyentes.html"
    model = Contribuyente
    context_object_name = "obj"
    login_url = "cundinas:login"

class Contribuyentes_update(LoginRequiredMixin, generic.UpdateView):
    template_name = "inv/Contribuyente.html"
    model = Contribuyente
    context_object_name = "obj"
    form_class = Contribuyente
    login_url = "cundinas:login"
    success_url = reverse_lazy("inv:Contribuyente")

class Grupo_new(LoginRequiredMixin, generic.CreateView):

    template_name = "inv/Registro_Grupo.html"
    model = Grupo
    context_object_name = "obj"
    form_class = Registro_grupo
    login_url = "cundinas:login"
    success_url = reverse_lazy("inv:Grupo")

    def form_valid(self, form):
        form.instance.user = self.request.user
        return super().form_valid(form)

class Grupo_list(LoginRequiredMixin, generic.ListView):
    template_name = "inv/Grupo.html"
    model = Grupo
    context_object_name = "obj"
    login_url = "cundinas:login"

class Grupo_update(LoginRequiredMixin, generic.UpdateView):
    template_name = "inv/Grupo.html"
    model = Grupo
    context_object_name = "obj"
    form_class = Grupo
    login_url = "cundinas:login"
    success_url = reverse_lazy("inv:Grupo")
